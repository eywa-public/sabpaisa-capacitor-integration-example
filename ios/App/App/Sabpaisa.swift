//
//  SabpaisaSdk.swift
//  App
//
//  Created by Lokesh D on 03/02/23.
//

import Foundation
import Capacitor
import SabPaisa_IOS_Sdk

@objc(Sabpaisa)
public class Sabpaisa: CAPPlugin {
    
    var initUrl="https://sdkstaging.sabpaisa.in/SabPaisa/sabPaisaInit?v=1"
    var baseUrl="https://sdkstaging.sabpaisa.in"
    var transactionEnqUrl="https://stage-txnenquiry.sabpaisa.in"
    
    @objc func nativeSdkCall(_ call: CAPPluginCall) {
        let value = call.getString("value") ?? ""
        call.resolve(["value": value])
        
        
        let bundle = Bundle(identifier: "com.eywa.ios.SabPaisa-IOS-Sdk")
        let storyboard = UIStoryboard(name: "Storyboard", bundle: bundle)
        
        
        let secKey="kaY9AIhuJZNvKGp2"
        let secInivialVector="YN2v8qQcU3rGfA1y"
        let transUserName="rajiv.moti_336"
        let transUserPassword="RIADA_SP336"
        let clientCode="TM001"
        
        DispatchQueue.main.async{
        
        let vc = storyboard.instantiateViewController(withIdentifier: "InitialLoadViewController_Identifier") as! InitialLoadViewController
            vc.sdkInitModel=SdkInitModel(firstName: "eywa", lastName: "solution", secKey: secKey, secInivialVector: secInivialVector, transUserName: transUserName, transUserPassword: transUserPassword, clientCode: clientCode, amount: Float("200")!,emailAddress: "sabpaisa@gmail.com",mobileNumber: "9734323221",isProd: false,baseUrl: self.baseUrl, initiUrl: self.initUrl,transactionEnquiryUrl: self.transactionEnqUrl)
        
        
        vc.callback =  { (response:TransactionResponse)  in
            print("---------------Final Response To USER------------")
            //self.sdkResponse.text = "Response : \(String(describing: response.status))"
            vc.dismiss(animated: true)
        }
        
            self.bridge?.viewController?.present(vc, animated: true,completion: nil)
        }
    }
}
