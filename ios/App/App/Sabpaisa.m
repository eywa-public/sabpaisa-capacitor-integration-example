//
//  SabpaisaSdkPlugin.m
//  App
//
//  Created by Lokesh D on 03/02/23.
//

#import <Foundation/Foundation.h>
#import <Capacitor/Capacitor.h>

CAP_PLUGIN(Sabpaisa, "Sabpaisa",
    CAP_PLUGIN_METHOD(nativeSdkCall, CAPPluginReturnPromise);
)
